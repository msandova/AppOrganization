# App Criteria

The app criteria provide a guide for app maintainers and reviewers to access the need for app optimization and help to decide if an app is admissible for a particular app category.

## General App Criteria

The general criteria have to be considered for all apps. Additional criteria for specific categories are listed subsequently.

### Required

#### General Quality

- [ ] Basic features and functionality work as expected
- [ ] Easy to get started on first run
- [ ] User reviews are generally positive. No repeated complaints about critical issues.
- [ ] No known issues that can lead to data loss

#### Maintenance

- [ ] Evidence of recent development activity
- [ ] Confidence in the maintainers' ability to reliably maintain the project
- [ ] Being careful about inflicting harm like data loss on users
- [ ] Keeping up with maintenance to at least using a supported runtime on Flathub

#### Technology Basis

- [ ] Uses GTK 4 + Libadwaita
- [ ] Uses the GNOME runtime on Flathub

#### Design

- [ ] Follows the [Human Interface Guidelines](https://developer.gnome.org/hig/)
- [ ] Dark mode works
    - [ ] Contrast is maintained everywhere (no dark font color on dark background etc.)
    - [ ] No white surfaces (apart from external media etc)

#### Content

- [ ] Does not contradict the intentions of the [GNOME Code of Conduct](https://conduct.gnome.org/)
- [ ] Does not promote cryptocurrencies or related services
    - *This would for example be the case if the app is focused on cryptocurrencies but not if the app also supports features around cryptocurrencies without those being the priority.*

#### Legal

- [ ] Has an [OSI-approved license](https://opensource.org/licenses)
- [ ] No contributor license agreement (CLA)
- [ ] The software must work without installing proprietary software

#### Metadata

- [ ] App metainfo is valid (`appstreamcli validate ./path/to/<app-id>.metainfo.xml` passes)
- [ ] App name follows [guidelines](https://developer.gnome.org/hig/guidelines/app-naming.html)
- [ ] App icon follows [guidelines](https://developer.gnome.org/hig/guidelines/app-icons.html)
- [ ] Screenshot quality ([more info](https://gitlab.gnome.org/GNOME/Initiatives/-/wikis/Update-App-Screenshots))
- [ ] "Summary" quality ([more info](https://gitlab.gnome.org/GNOME/Initiatives/-/wikis/App-Metadata#summary))
- [ ] .doap-file in repository exists and is up-to-date ([more info](https://gitlab.gnome.org/World/apps-for-gnome/-/blob/main/METADATA.md#doap))
- [ ] Hardware support information [is present](https://gitlab.gnome.org/GNOME/gnome-software/-/wikis/Software-metadata#how-to-add-missing-hardware-information)
- [ ] Content rating information [is present](https://gitlab.gnome.org/GNOME/gnome-software/-/wikis/Software-metadata#how-to-add-missing-content-rating-information)

#### Accessibility

- [ ] Can navigate to all elements via keyboard and use them
- [ ] Uses standard keyboard shortcuts ([more info](https://developer.gnome.org/hig/reference/keyboard.html))
- [ ] High-contrast mode works
- [ ] Large text mode works
- [ ] Sufficient contrast between content and background
- [ ] Works with Orca (activate via `Alt+Super+S`)
    - [ ] Image buttons have a tooltip

#### Repository

- [ ] Passing CI builds of the app
- [ ] Public issue tracker and code repository
    - *The repository does *not* need to be hosted on GNOME's GitLab instance.*

### Recommended

- [ ] Builds Flatpak in Builder out of the box
- [ ] Build system is Meson, Automake, or CMake
- [ ] Making contributors aware that the [GNOME Code of Conduct](https://conduct.gnome.org/) applies to the project
    - *For example linking to the Code of Conduct in the project's README*

## Circle App Criteria

- [ ] Fulfills the "General App Criteria"
- [ ] Available on Flathub
  - [ ] Available for x86\_64 and aarch64
- [ ] No GNOME branding
    - *The app may not use GNOME in its name or its author field and may not promote the app as official GNOME software. However, after inclusion into GNOME Circle, the maintainers are encouraged to promote the app as being part of "GNOME Circle", using [the Circle branding](https://gitlab.gnome.org/Teams/Circle/-/tree/main/assets/button).*
- [ ] App ID is valid
    - Valid app IDs are
        - IDs of the form `<public-suffix>.<something>.<app>`, where `https://<something>.<public-suffix>/.well-known/org.flathub.VerifiedApps.txt` can be controlled by the user
        - IDs of the form `com.github.<user>.<app>`, `io.github.<user>.<app>`, `com.gitlab.<user>.<app>`, or `io.gitlab.<user>.<app>`.
        - IDs that are on the [list of legacy apps](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/data/registered-app-ids.yml)
    - Invalid app IDs
        - Does not have the form `org.gnome.<app>`, expect the cases listed in the valid app IDs above
        - Follows [Flatpak requirements](https://docs.flatpak.org/en/latest/conventions.html#application-ids) like replacing `-` with `_` in app IDs
- [ ] *Ignore for new apps. Only applied beginning with first regular review:* The app support internationalization and is connected to some localization infrastructure.

## Incubation Criteria

*Note: These Criteria are not relevant for GNOME Circle.*

- [ ] There is demand for an app with this functionality in Core/Development.
- [ ] The app is based on existing design, if applicable.
- [ ] The maintainers ideally already got in touch with the Release or Design Team before the submission.
- [ ] The app and the maintainers are on track to fulfilling the "Core App and Development Tool Criteria."

## Core App and Development Tool Criteria

*Note: These Criteria are not relevant for GNOME Circle.*

*Note: While all criteria apply for GNOME Development tools, the general quality requirements are not as high as for GNOME Core apps.*

- [ ] Fulfills the "General App Criteria"
- [ ] Follows the ideas of the [Official GNOME App Definition](OfficialAppDefinition.md).
- [ ] Available on Flathub if technically viable
- [ ] Builds on x86\_64 and aarch64
- [ ] CI produces nightly Flatpaks if technically viable
- [ ] Dependencies are approved by Release Team
- [ ] Follows the GNOME release schedule and versioning scheme
- [ ] Translation is available on l10n.gnome.org
