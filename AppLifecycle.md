# Official App Processes

GNOME's official apps are those which belong to the GNOME core and development app sets (see the [official app definition](OfficialAppDefinition.md)). This page describes the processes through which apps are added and removed from these sets. It is intended as a reference document for those who are participating in the addition and removal of official GNOME apps, as well as developers who are interested in making their app part of an official app set.

Apps are added to GNOME's official app sets by the Release Team, who work in partnership with the app's maintainers and the Design Team.

## App addition

Official apps are required to meet the [app criteria](AppCriteria.md) and go through the addition procedures as described in this section. Apps are usually only added to GNOME's official app sets after going through the incubation process. However, in some cases the reviewers can decide that the app is mature and uncontroversial enough to skip the incubation process. For example, if the app is already in Circle. In this case, the app should apply to GNOME Core/Development directly.

### Incubation process

The incubation process allows apps to get early feedback from the release and design teams, and to coordinate their inclusion with the rest of the project.

Information for apps applying to incubation:

- The barrier to entering GNOME core is usually much higher than entering GNOME Circle. Core apps are not regularly exchanged, and the set of core apps is not extended regularly. Please check if a [GNOME Circle submission](https://gitlab.gnome.org/Teams/Circle) could be a better fit.
- Submission of a core app to the incubation phase should usually be based on existing [design mockups](https://gitlab.gnome.org/Teams/Design/app-mockups). If you have an idea for an app not covered by current Design Team proposals, please contact the Design Team first.
- The maintainers of the app should ideally get in touch with the design and/or release teams in advance of beginning the work on this proposal.
- The app should target to fulfill the [Core App or Development Tool Definition](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/OfficialAppDefinition.md) and the corresponding [App Criteria](https://gitlab.gnome.org/Teams/Releng/AppOrganization/-/blob/main/AppCriteria.md). You should be familiar with all the requirements when submitting an app for incubation. However, the app submitted for incubation does not need to fulfill all of those criteria yet. The requirement for incubation is a minimal viable product (MVP) that can be worked on during the incubation stage.

Incubation has the following stages:

1.  Application to the Incubator
    - The app's maintainers apply to have it added to incubation by opening [an issue against the Incubator project](https://gitlab.gnome.org/Incubator/Submission/-/issues/new).
    - The app is reviewed (see [review procedure](ReviewProcedures.md)). If it meets the [incubation criteria](AppCriteria.md) it is added to the Incubator.
2.  Inclusion in the Incubator
    - If the app is supposed to potentially replace another Core/Development app, inform the maintainers of the existing app about the now incubating app.
    - App is moved to `Incubator/` namespace.
    - The app ID is usually changed to `org.gnome.<codename>`. *This app ID can be retained, even if the project is rejected or at some point remove from GNOME core.*
    - The addition to the Incubator is publicly announced
3.  Incubation stage
    - The app's contributors work towards a shippable application that fulfills the [core app criteria](AppCriteria.md).
    - Feedback from stakeholders like distributions is collected.
    - The app is potentially published on Flathub under the code name for feedback.

### Adding official apps

When it is judged that the app is ready to be added to core:

4.  Submit to GNOME Core
    - The app maintainer(s) open an issue under [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/issues) for review.
    - The Release and Design Team will review the app based on the [core app criteria](AppCriteria.md).
5.  Once accepted into Core, the app is move to the `GNOME/<codename>` namespace

If the app is rejected in the incubation phase it will be removed from `/Incubation`. The maintainers can decide to continue the project under the codename or any other name elsewhere.

## App removal

Core apps can be removed for the following reasons:

1.  **Out of scope**: their features are no longer considered to be part of the [Core App Definition](OfficialAppDefinition.md)
    - This requires an overview of which features would no longer be part of Core after the removal. For all features, it has to be considered if they are not required for Core anymore or if keeping the app would be a larger problem than losing those features.
    - A tracking issue must be created not later than the first month of the development cycle.
2.  **Replacement**: they are replaced by a new app. In this case the removal is tracked as part of the Incubation process, which, for app replacement, has to be accepted within the first month of the development cycle.
3.  **App does not meet criteria**: the app is identified as being a candidate for removal in the process of a regular or irregular review (see below).
4.  **Conflict with maintainers**: if the app Maintainers and Release Team or Design Team arrive irreconcilable differences about the app and the maintainers want to continue app outside of Core
    - The app can be replaced with a new app.
    - The app can be forked and used with a new code name in Core. Form of attribution should be clarified with original maintainers.
    - Since the app is owned by its maintainers, it cannot be overtaken as is.

### App does not meet criteria

When this happens:

- The first step is for reviewers to contact the app maintainers to discuss concerns and next steps. Where possible, consensus with existing maintainers and contributors is desirable.
- A ticket to track the app removal should be created once this meeting with existing maintainers has occurred, but not before.
- Apps are typically scheduled for final removal no sooner than six months after being identified as removal candidates. However, this schedule can be shortened in exceptional cases.
- Usually, the priority will still be on fixing the problem within the project. Reviewers and the general community can for example consider the following options:
    - Trying to add new maintainers
    - Publicly calling for contributions on TWIG, Planet GNOME, and social media to fix the problems at hand
    - Advising the maintainers in handling conceptual and technical issues
    - Providing missing designs
   
For completeness, it should be noted that maintainers are generally not entitled to help from the community.

Plans to remove an app should be broadcast to the community, as well as being communicated directly to relevant teams and individuals.

A final evaluation should be completed prior to the removal of the app.

### Removal procedure

- In the event that an application is removed from core, the maintainers should create at least one release with a non-generic app name, reverting to the codename or something else.
- The app ID of the form `org.gnome.<codename>` can be retained by the project after the removal from Core.
